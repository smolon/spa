import React from 'react';
import './Book.css';
import styles from './Book.module.css';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import { createMuiTheme, withStyles, ThemeProvider } from '@material-ui/core/styles';
import { green, purple } from '@material-ui/core/colors';

const useStyles = makeStyles({
	card   : {
		minWidth : 275
	},
	bullet : {
		display   : 'inline-block',
		margin    : '0 2px',
		transform : 'scale(0.8)'
	},
	title  : {
		fontSize : 14
	},
	pos    : {
		marginBottom : 12
	}
});

const Book = (props) => {
	const classes = useStyles();
	const bull = <span className={classes.bullet}>•</span>;
	const theme = createMuiTheme({
		palette : {
			primary : green
		}
	});
	return (
		<div className={styles.article}>
			{/* <h2> {props.title} </h2>Autor: {props.author}
			<br />
			Rok wydania: {props.year}
			<br />
			Wydawnictwo: {props.publisher}
			<br />
			Cena: {props.price}
			<br />
			<button onClick={props.updateClick}>Edytuj książkę</button>
			<button onClick={props.deleteClick}>Usuń książkę</button>
			<br />
			<br /> */}
			<Card className={classes.card} variant="outlined">
				<CardContent>
					<Typography variant="h5" component="h2">
						{props.title}
					</Typography>
					<Typography className={classes.pos} color="textSecondary">
						Autor: {props.author}
					</Typography>
					<Typography variant="body2" component="p">
						Rok wydania: {props.year}
					</Typography>
					<Typography variant="body2" component="p">
						Wydawnictwo: {props.publisher}
					</Typography>
					<Typography variant="body2" component="p">
						Cena: {props.price}
					</Typography>
				</CardContent>
				<CardActions>
					<Button
						variant="contained"
						color="primary"
						startIcon={<EditIcon />}
						size="small"
						onClick={props.updateClick}
					>
						Edytuj książkę
					</Button>
					<Button
						variant="contained"
						color="secondary"
						startIcon={<DeleteIcon />}
						size="small"
						onClick={props.deleteClick}
					>
						Usuń książkę
					</Button>
				</CardActions>
			</Card>
		</div>
	);
};

export default Book;
