import React, { Component } from 'react';
import './App.css';
import Book from './Book/Book';
import styles from './App.module.css';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import FilterResults from 'react-filter-search';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import axios from 'axios';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import SimpleReactValidator from 'simple-react-validator';
import $ from 'jquery';
import { makeStyles, ThemeProvider, useTheme, createMuiTheme } from '@material-ui/core/styles';
import SearchIcon from '@material-ui/icons/Search';
import { InputAdornment } from '@material-ui/core';
import { green, purple } from '@material-ui/core/colors';
//wyglad wyszukiwarki\
//pozycja wyszukiwarki
//walidacja pol w formularzu
//api
//naprawienie pol przy edycji jakiejs ksiazki bo sie te okienka nie robia wieksze i sie napisy zlewaja
class App extends Component {
	constructor(props) {
		super(props);
		this.validator = new SimpleReactValidator();
		this.state = {
			books            : [],
			authors          : [],
			showBooks        : false,
			updateButtonShow : false,
			index            : null,
			searchTerm       : '',
			value            : '',
			data             : [],
			sort             : ''
		};
	}
	state = {
		books            : [
			// {
			// 	id        : 'asdad213',
			// 	title     : 'wwwww',
			// 	author    : 'Mieszko I',
			// 	publisher : 'publisher1',
			// 	year      : '1997',
			// 	price     : '200'
			// },
			// {
			// 	id        : 'asqwecz13',
			// 	title     : 'ccccc',
			// 	author    : 'Adam Solon',
			// 	publisher : 'publisher2',
			// 	year      : '1992',
			// 	price     : '400'
			// },
			// {
			// 	id        : 'czxve',
			// 	title     : 'iiii',
			// 	author    : 'Kuba',
			// 	publisher : 'publisher3',
			// 	year      : '1993',
			// 	price     : '100'
			// },
			// {
			// 	id        : 'aczbdd',
			// 	title     : 'bbbbb',
			// 	author    : 'Marios',
			// 	publisher : 'kasztan',
			// 	year      : '1994',
			// 	price     : '300'
			// }
		],
		authors          : [],
		showBooks        : true,
		updateButtonShow : false,
		index            : null,
		searchTerm       : '',
		value            : '',
		data             : [],
		sort             : '',
		title            : '',
		author           : '',
		publisher        : '',
		year             : '',
		price            : '',
		formErrors       : {
			title     : '',
			author    : '',
			publisher : '',
			year      : '',
			price     : ''
		},
		titleValid       : false,
		authorValid      : false,
		publisherValid   : false,
		yearValid        : false,
		priceValid       : false,
		formValid        : false,
		intervalId       : 0
	};

	componentDidMount() {
		// let one = 'http://localhost:3000/books';
		// let two = 'http://localhost:3000/authors';
		// const requestOne = axios.get(one);
		// const requestTwo = axios.get(two);
		// axios
		// 	.all([ requestOne, requestTwo ])
		// 	.then(
		// 		axios.spread((...responses) => {
		// 			console.log('>>>>>>>>>' + responses[0]);
		// 			this.setState({ books: responses[0].data });
		// 			this.setState({ authors: responses[1].data });
		// 		})
		// 	)
		// 	.catch((error) => {
		// 		console.log('nie udalo sie pobrac danych z api' + error);
		// 	});
		fetch('http://localhost:3000/authors').then((response) => response.json()).then(
			(json) => {
				this.setState({ authors: json });
				console.log(this.state.authors);
			},
			(error) => {
				console.log('nie udalo sie pobrac danych z api' + error);
			}
		);
		fetch('http://localhost:3000/books').then((response) => response.json()).then(
			(json) => {
				this.setState({ books: json });
				console.log(this.state.books);
			},
			(error) => {
				console.log('nie udalo sie pobrac danych z api' + error);
			}
		);
	}
	$() {
		$.fn.inputFilter = function(inputFilter) {
			return this.on('input keydown keyup mousedown mouseup select contextmenu drop', function() {
				if (inputFilter(this.value)) {
					this.oldValue = this.value;
					this.oldSelectionStart = this.selectionStart;
					this.oldSelectionEnd = this.selectionEnd;
				}
				else if (this.hasOwnProperty('oldValue')) {
					this.value = this.oldValue;
					this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
				}
			});
		};
	}
	$() {
		$('#title').keypress(function(e) {
			var txt = String.fromCharCode(e.which);
			console.log(txt + ' : ' + e.which);
			if (!txt.match(/[A-Za-z]/)) {
				return false;
			}
		});
	}
	// $('#name').inputFilter(function(value) {
	// 	return /^[a-z\u00c0-\u024f]*$/i.test(value);
	// });
	// $('#price, #count').inputFilter(function(value) {
	// 	return /^\d*[.]?\d*$/.test(value);
	// });
	handleChangeFilter = (event) => {
		const { value } = event.target;
		this.setState({ value });
	};
	deleteArticleHandler = (articleIndex) => {
		// const articles = this.state.articles.slice();
		const books = [ ...this.state.books ];
		axios.delete('http://localhost:3000/books/' + books[articleIndex].id).then((respone) => {
			console.log(respone);
		});
		books.splice(articleIndex, 1);
		this.setState({ books: books });
	};
	updateBookHandler = (bookIndex) => {
		//window.scrollTo(0, 0);
		this.scrollToTop();
		const updateButtonVisible = this.state.updateButtonShow;
		this.setState({ updateButtonShow: true });
		const books = this.state.books;
		this.setState({ index: bookIndex });
		document.getElementById('title').value = books[bookIndex].title;
		document.getElementById('author').value = books[bookIndex].author;
		document.getElementById('publisher').value = books[bookIndex].publisher;
		document.getElementById('year').value = books[bookIndex].year;
		document.getElementById('price').value = books[bookIndex].price;
	};

	addBookToState = () => {
		const title = document.getElementById('title').value;
		const author = document.getElementById('author').value;
		const publisher = document.getElementById('publisher').value;
		const year = document.getElementById('year').value;
		const price = document.getElementById('price').value;
		const id = Math.floor(new Date() / 1000);
		console.log(id);
		const book = {
			id        : id,
			title     : title,
			author    : author,
			publisher : publisher,
			year      : year,
			price     : price
		};
		const books = [ ...this.state.books ];
		books.push(book);
		this.setState({ books: books });

		document.getElementById('title').value = '';
		document.getElementById('author').value = '';
		document.getElementById('publisher').value = '';
		document.getElementById('year').value = '';
		document.getElementById('price').value = '';
		axios.post('http://localhost:3000/books', book).then((response) => {
			console.log(response);
		});
		if (!this.checkIfAuthorExists(book.author)) {
			const author = {
				id   : id,
				name : book.author
			};
			const authors = [ ...this.state.authors ];
			authors.push(author);
			this.setState({ authors: authors });
			axios.post('http://localhost:3000/authors', author).then((response) => {
				console.log(response);
			});
		}
		else {
			console.log('Nie dodano autora ponieważ juz taki istnieje');
		}
	};
	scrollStep() {
		if (window.pageYOffset === 0) {
			clearInterval(this.state.intervalId);
		}
		window.scroll(0, window.pageYOffset - this.props.scrollStepInPx);
	}

	scrollToTop() {
		let intervalId = setInterval(this.scrollStep.bind(this), '0');
		this.setState({ intervalId: intervalId });
	}
	checkIfAuthorExists = (authorName) => {
		const authors = this.state.authors;
		for (let i = 0; i < authors.length; i++) {
			if (authors[i].name === authorName) return true;
		}
		return false;
	};
	updateBookToState = () => {
		const index = this.state.index;
		const books = [ ...this.state.books ];
		const title = document.getElementById('title').value;
		const author = document.getElementById('author').value;
		const publisher = document.getElementById('publisher').value;
		const year = document.getElementById('year').value;
		const price = document.getElementById('price').value;
		const book = {
			id        : books[index].id,
			title     : title,
			author    : author,
			publisher : publisher,
			year      : year,
			price     : price
		};
		books.splice(index, 1, book);
		this.setState({ books: books });
		axios.put('http://localhost:3000/books/' + books[index].id, book).then((response) => {
			console.log(response);
		});
		document.getElementById('title').value = '';
		document.getElementById('author').value = '';
		document.getElementById('publisher').value = '';
		document.getElementById('year').value = '';
		document.getElementById('price').value = '';
		this.setState({
			updateButtonShow : false
		});
	};
	toggleBooksHandler = () => {
		const doesShow = this.state.showBooks;
		this.setState({ showBooks: !doesShow });
	};
	compareTitles(a, b) {
		const titleA = a.title.toUpperCase();
		const titleB = b.title.toUpperCase();
		let comparison = 0;
		if (titleA > titleB) {
			comparison = 1;
		}
		else if (titleA < titleB) {
			comparison = -1;
		}
		return comparison;
	}
	comparePricesAscending(a, b) {
		const priceA = Number(a.price);
		const priceB = Number(b.price);
		let comparison = 0;
		if (priceA > priceB) {
			comparison = 1;
		}
		else if (priceA < priceB) {
			comparison = -1;
		}
		return comparison;
	}

	comparePricesDescending(a, b) {
		const priceA = Number(a.price);
		const priceB = Number(b.price);
		let comparison = 0;
		if (priceA > priceB) {
			comparison = 1;
		}
		else if (priceA < priceB) {
			comparison = -1;
		}
		return comparison * -1;
	}
	compareAuthors(a, b) {
		const authorA = a.author.toUpperCase();
		const authorB = b.author.toUpperCase();
		let comparison = 0;
		if (authorA > authorB) {
			comparison = 1;
		}
		else if (authorA < authorB) {
			comparison = -1;
		}
		return comparison;
	}
	setSort = (value) => {
		this.setState({ sort: value });
		const originalBooks = [ ...this.state.books ];
		let books = this.state.books;
		if (this.state.value === 0) {
			this.setState({ books: originalBooks });
		}
		else if (value === 1) {
			books.sort(this.comparePricesAscending);
			this.setState({ books: books });
			console.log(this.state.sort);
		}
		else if (value === 2) {
			books.sort(this.comparePricesDescending);
			this.setState({ books: books });
		}
		else if (value === 3) {
			books.sort(this.compareTitles);
			this.setState({ books: books });
		}
		else if (value === 4) {
			books.sort(this.compareAuthors);
			this.setState({ books: books });
		}
	};
	handleChange = (event) => {
		this.setSort(event.target.value);
	};
	getAuthorName = (bookIndex) => {
		// console.log(this.state.authors);
		// const authors = this.state.authors.splice();
		// console.log(authors);
		// const author = authors.find((x) => x.id === author_id);
		// return author.name;
		const book = this.state.books[bookIndex];
		const author_id = Number(book.author_id);
		const authors = this.state.authors;
		for (let i = 0; i < authors.length; i++) {
			if (authors[i].id === author_id) return authors[i].name;
		}
		//const author = authors.find((x) => x.id === author_id);
		return 'author.name';
	};
	getAuthorNameById = (authorId) => {
		const authors = this.state.authors;
		for (let i = 0; i < authors.length; i++) {
			if (authors[i].id === authorId) {
				return authors[i].name;
			}
		}
		return null;
	};
	findObjectByKey(array, key, value) {
		for (var i = 0; i < array.length; i++) {
			if (array[i][key] === value) {
				return array[i];
			}
		}
		return null;
	}
	handleUserInput(e) {
		const name = e.target.name;
		const value = e.target.value;
		this.setState({ [name]: value }, () => {
			this.validateField(name, value);
		});
	}
	validateField(fieldName, value) {
		let fieldValidationErrors = this.state.formErrors;
		let emailValid = this.state.emailValid;
		let passwordValid = this.state.passwordValid;
		let titleValid = this.state.titleValid;
		const formValid = this.state.formValid;

		switch (fieldName) {
			case 'email':
				emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
				fieldValidationErrors.email = emailValid ? '' : ' is invalid';
				break;
			case 'password':
				passwordValid = value.length >= 6;
				fieldValidationErrors.password = passwordValid ? '' : ' is too short';
				break;
			case 'title':
				if (titleValid > 2) {
					this.setState({ formValid: true });
				}
			default:
				break;
		}
		this.setState(
			{
				formErrors    : fieldValidationErrors,
				emailValid    : emailValid,
				passwordValid : passwordValid
			},
			this.validateForm
		);
	}

	validateForm() {
		this.setState({ formValid: this.state.emailValid && this.state.passwordValid });
	}
	render() {
		const darkTheme = createMuiTheme({
			palette : {
				type : 'dark'
			}
		});
		const { data, value } = this.state;
		let books = null;
		//let buttonStyles = [ styles.toggleButton ];
		const searchResult = (
			<FilterResults
				value={value}
				data={this.state.books}
				renderResults={(results) => (
					<div>
						{results.map((el, index) => (
							<div>
								<Book
									title={el.title}
									author={el.author}
									key={el.id}
									publisher={el.publisher}
									year={el.year}
									price={el.price}
								/>
							</div>
						))}
					</div>
				)}
			/>
		);
		let authors = this.state.authors;
		if (authors.length > 0) {
			//console.log(authors[0]);
		}
		books = this.state.books.map((book, index) => {
			//console.log(this.state.books);
			//console.log(this.state.authors);
			//const authorName = this.state.authors.find((x) => x.id === book.author_id);
			//const authorName = this.state.authors[1].id;
			//console.log(authors[0]);
			const authorName = JSON.parse(JSON.stringify(this.state.authors));
			const authorIndex = authorName.findIndex((x) => x.id === book.author_id);
			//console.log(authorIndex);
			//console.log(authorName);
			// if (this.state.authors.length > 0) {
			// 	console.log(this.state.authors);
			// 	authorName = this.state.authors;
			// }
			//findAuthor()
			//if (this.state.showBooks) {
			// 	if (this.state.authors > 0) {

			return (
				<Book
					title={book.title}
					author={book.author}
					//author={authorName.find((x) => x.id === book.author_id).name}
					//author={book.author_id}
					deleteClick={() => this.deleteArticleHandler(index)}
					updateClick={() => this.updateBookHandler(index)}
					key={book.id}
					publisher={book.publisher}
					year={book.year}
					price={book.price}
				/>
			);
			//}
			//}
		});
		if (this.state.updateButtonShow) {
		}
		const articleStyles = [];

		if (this.state.books.length === 1) {
			articleStyles.push('OneArticle');
		}

		if (this.state.books.length >= 4) {
			articleStyles.push('GreenArticles');
		}
		else {
			articleStyles.push('OrangeArticles');
		}
		const theme2 = createMuiTheme({
			palette : {
				primary : green
			}
		});
		const addButton = (
			<ThemeProvider theme={theme2}>
				<Button
					style={{ margin: 10 }}
					id="addButton"
					onClick={this.addBookToState}
					variant="contained"
					color="primary"
				>
					Dodaj książkę
				</Button>
			</ThemeProvider>
		);
		const updateButton = (
			<Button
				style={{ margin: 10 }}
				id="addButton"
				onClick={this.updateBookToState}
				variant="contained"
				color="primary"
			>
				Aktualizuj książkę
			</Button>
		);
		const theme = createMuiTheme();

		return (
			<div className={styles.App}>
				<ThemeProvider theme={darkTheme}>
					<div className={styles.form}>
						<div className={styles.MyForm}>
							<div className="container">
								<form autoComplete="off">
									<div>
										<TextField
											value={this.state.title}
											required
											style={{ margin: 8 }}
											id="title"
											label="Tytuł"
											name="title"
											InputLabelProps={{ shrink: true }}
										/>
									</div>
									<div>
										<TextField
											value={this.state.author}
											required
											style={{ margin: 8 }}
											id="author"
											label="Autor"
											name="author"
											InputLabelProps={{ shrink: true }}
										/>
									</div>
									<div>
										<TextField
											value={this.state.publisher}
											required
											style={{ margin: 8 }}
											id="publisher"
											label="Wydawnictwo"
											name="publisher"
											InputLabelProps={{ shrink: true }}
										/>
									</div>
									<div>
										<TextField
											value={this.state.year}
											required
											style={{ margin: 8 }}
											id="year"
											label="Rok wydania"
											name="year"
											InputLabelProps={{ shrink: true }}
										/>
									</div>
									<div>
										<TextField
											value={this.state.price}
											required
											style={{ margin: 8 }}
											id="price"
											label="Cena"
											name="price"
											InputLabelProps={{ shrink: true }}
										/>
									</div>
								</form>
							</div>
							{this.state.updateButtonShow ? updateButton : addButton}
							<br />
						</div>
						{/* <button className={buttonStyles.join(' ')} onClick={this.toggleArticlesHandler}>
					Dodaj książkę
				</button> */}
						{/* <div className={articleStyles.join(' ')}>{books}</div> */}
					</div>
					<div className={styles.mainCol}>
						<div className={styles.sort}>
							<FormControl variant="filled">
								<InputLabel id="demo-simple-select-filled-label">Sortuj</InputLabel>
								<Select
									labelId="demo-simple-select-filled-label"
									id="demo-simple-select-filled"
									value={this.state.sort}
									onChange={this.handleChange}
									style={({ width: 14 + 'em' }, { minWidth: 200 + 'px' })}
								>
									<MenuItem value={0}>
										<em>brak</em>
									</MenuItem>
									<MenuItem value={1}>Cena: od najniższej</MenuItem>
									<MenuItem value={2}>Cena: od najwyższej</MenuItem>
									<MenuItem value={3}>Tytuł: od A do Z</MenuItem>
									<MenuItem value={4}>Autor: od A do Z</MenuItem>
								</Select>
							</FormControl>
						</div>
						<div>{this.state.value ? searchResult : books}</div>
					</div>
					<div>
						{/* <h2>Filtruj: </h2>
						<input type="text" value={value} onChange={this.handleChangeFilter} /> */}
						<TextField
							value={value}
							margin="normal"
							label="Filtruj"
							onChange={this.handleChangeFilter}
							InputProps={{
								startAdornment : (
									<InputAdornment position="start">
										<SearchIcon />
									</InputAdornment>
								)
							}}
						/>
					</div>
				</ThemeProvider>
			</div>
		);
	}
}

export default App;
